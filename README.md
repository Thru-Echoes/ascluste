# Adaptive Search Clustering Ensemble 

A package of clustering utility functions and an adaptive searching ensemble clustering algorithm to find optimal parameters.   

Our methods involve the development and use of a novel machine learning ensemble algorithm that dramatically improved both statistical stability and domain-based interpretability of clustering spatio-temporal data. Statistical stability was measured by quantifying a reduction in variance, avoidance of overfitting, and quantifyiable metrics of how distinct objects were assigned across clusters. These statistical metrics were compared to and out-performed conventional clustering algorithms, e.g. k-means, k-medoids, and hierarchical clustering. In addition, final cluster assignments offered a high degree of interpretability by domain-experts in hydro-bio-geo-chemi-sciences! A generic implementation of our novel ensemble algorithm and various utility functions can be found in the AsClustE Python package (in development: https://gitlab.com/Thru-Echoes/ascluste). 

AsClustE, adaptive-search clustering ensemble, attempts to cluster highly variant and relatively high dimensional (i.e. many features compared to observations + amount of variance) data through a two-prong approach. Akind to the popular approach of bootstrapping aggregation (i.e. bagging method), various cluster algorithms with a range of parameters are ran iteratively and optimized towards a loss function based on a cluster representation metric we modeled. Secondly, the various algorithm results are aggregated through an ensembling function that produces the final cluster assignments.   



## Overview of ensembling 

Our ensemble algorithm uses adaptive searching that evokes a custom loss function, minimizing distance between median feature values and maximizing average Silhouette values, to assign observations across concurrently computed cluster models. In its most generic form, this custom multi-tier compute pipeline can be assumed to be more robust and account for potential pitfalls from the data than any given algorithm used within the ensemble. In its specific implementation within our methods, ensemble model results formed emergent observation assignments and greater insight into the data compared to any one of the three cluster algorithms alone.  

#### Step 1: Multi-run of algorithm 

Cluster centroids (or means) are reassigned for a single iteration of a clustering algorithm (**e.g. k-means, k-medoids**). In addition, for each observation, silhouette coefficients are calculated as a measure of how well each observation was assigned to its respective cluster (**i.e. how distinct clusters formed**). For each iteration, if silhouette coefficients are larger than the previous run, the cluster centroids are reassigned to the existing run. This provides a way to find optimal models based on maximizing silhouette values.  

#### Step 2: Ensemble loss function 

    p(x_i) = [(Pmed_1 - px_i1), (Pmed_2 - px_i2), ..., (Pmed_9 - px_i9)]; where x_i is single observation 

For each observation, p(x) function will return a n-element array of differences between median feature value of the representative cluster and the sample's feature value. In the example above a 9-feature dataset is shown.

The values of p(x) are then weighted by a variance difference factor. 

Final clustering is based on both silhouette coefficients and relative distance from representative cluster medians and variances. These two metrics give a robust measurement of the quality of both the clusters generated and the assignments of observations to clusters. 

