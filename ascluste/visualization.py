
# FIXME: add dependency imports (e.g. "import seaborn as sns")

def show_beautiful_bivariate_groupings(dat, color_by = False):
    g = sns.PairGrid(dat, hue = color_by)
    g.map_diag(plt.hist)
    g.map_offdiag(plt.scatter)
    g.add_legend();
    
def show_correlation_heatmap(corr_matrix):
    fig, ax = plt.subplots(figsize = (12, 12))  
    ax = sns.heatmap(corr_matrix, annot = True, linewidths = 1, ax = ax, cmap = "Reds")
    fig = ax.get_figure()

def show_col_boxplot(df, col):
    fig = plt.figure(figsize = (8, 8))
    ax = fig.gca()
    df.boxplot(column = col, ax = ax)
    ax.set_title("Filtered column: " + col)
    #ax.set_xlabel(col)
    ax.set_ylabel("Values")
    return "show_col_boxplot() for " + col

def get_each_col_boxplot(df, col_names):
    ## List comprehension (call fn on each item in list)
    # [fn_to_call() for item in the_list]
    return [show_col_boxplot(df, col_name) for col_name in col_names]

def get_cluster_boxplot(clusters,pollutant, data):
    return sns.boxplot(x=clusters, y=pollutant, data=data)

def get_cluster_count(data, clustercolumn):
    return data.groupby(clustercolumn).agg(['count']).iloc[:,0]

def viz_silhouette_for_kmeans(X):
    range_n_clusters = [2, 3, 4, 5, 6, 7, 8]

    for n_clusters in range_n_clusters:
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.set_size_inches(18, 7)

        # The 1st subplot is the silhouette plot
        # The silhouette coefficient can range from -1, 1
        ax1.set_xlim([-1, 1])
        ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])
        
        # Kmeans
        clusterer = KMeans(n_clusters = n_clusters)
        cluster_labels = clusterer.fit_predict(X)

        # The silhouette_score gives the average value for all the samples.
        # This gives a perspective into the density and separation of the formed
        # clusters
        silhouette_avg = silhouette_score(X, cluster_labels)
        print("For n_clusters =", n_clusters,
              "The average silhouette_score is :", silhouette_avg)

        # Compute the silhouette scores for each sample
        sample_silhouette_values = silhouette_samples(X, cluster_labels)

        y_lower = 10
        for i in range(n_clusters):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = \
                sample_silhouette_values[cluster_labels == i]

            ith_cluster_silhouette_values.sort()

            size_cluster_i = ith_cluster_silhouette_values.shape[0]
            y_upper = y_lower + size_cluster_i

            color = cm.nipy_spectral(float(i) / n_clusters)
            ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,
                          facecolor=color, edgecolor=color, alpha=0.7)

            # Label the silhouette plots with their cluster numbers at the middle
            ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
            y_lower = y_upper + 10  # 10 for the 0 samples

        ax1.set_title("The silhouette plot for the various clusters.")
        ax1.set_xlabel("The silhouette coefficient values")
        ax1.set_ylabel("Cluster label")

        # The vertical line for average silhouette score of all the values
        ax1.axvline(x=silhouette_avg, color="red", linestyle="--")
        ax1.set_yticks([])
        ax1.set_xticks([-1, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1])

        # 2nd Plot showing the actual clusters formed
        colors = cm.nipy_spectral(cluster_labels.astype(float) / n_clusters)
        ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                c=colors, edgecolor='k')

        # Labeling the clusters
        centers = clusterer.cluster_centers_
        ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                c="white", alpha=1, s=200, edgecolor='k')
        
        for i, c in enumerate(centers):
            ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                    s=50, edgecolor='k')

        ax2.set_title("The visualization of the clustered data.")
        ax2.set_xlabel("Feature space for the 1st feature")
        ax2.set_ylabel("Feature space for the 2nd feature")

        plt.suptitle(("Silhouette analysis for K-means "
                  "with n_clusters = %d" % n_clusters),
                 fontsize=14, fontweight='bold')

    plt.show()
