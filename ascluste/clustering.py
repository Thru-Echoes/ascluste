import pandas as pd
import numpy as np
from sklearn import metrics
from sklearn.cluster import DBSCAN
from sklearn.neighbors import NearestNeighbors
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from sklearn import preprocessing, manifold
import seaborn as sns; sns.set()
from scipy.cluster.hierarchy import dendrogram, linkage 
from scipy.cluster.hierarchy import fcluster
from sklearn.cluster import AgglomerativeClustering
import seaborn as sns
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score

from sklearn.model_selection import train_test_split
import random
from sklearn.metrics.pairwise import pairwise_distances

def kMedoids(D, k, tmax=100):
    # determine dimensions of distance matrix D
    m, n = D.shape

    if k > n:
        raise Exception('too many medoids')

    # find a set of valid initial cluster medoid indices since we
    # can't seed different clusters with two points at the same location
    valid_medoid_inds = set(range(n))
    invalid_medoid_inds = set([])
    rs,cs = np.where(D==0)
    # the rows, cols must be shuffled because we will keep the first duplicate below
    index_shuf = list(range(len(rs)))
    np.random.shuffle(index_shuf)
    rs = rs[index_shuf]
    cs = cs[index_shuf]
    for r,c in zip(rs,cs):
        # if there are two points with a distance of 0...
        # keep the first one for cluster init
        if r < c and r not in invalid_medoid_inds:
            invalid_medoid_inds.add(c)
    valid_medoid_inds = list(valid_medoid_inds - invalid_medoid_inds)

    if k > len(valid_medoid_inds):
        raise Exception('too many medoids (after removing {} duplicate points)'.format(
            len(invalid_medoid_inds)))

    # randomly initialize an array of k medoid indices
    M = np.array(valid_medoid_inds)
    np.random.shuffle(M)
    M = np.sort(M[:k])

    # create a copy of the array of medoid indices
    Mnew = np.copy(M)

    # initialize a dictionary to represent clusters
    C = {}
    for t in range(tmax):
        # determine clusters, i. e. arrays of data indices
        J = np.argmin(D[:,M], axis=1)
        for kappa in range(k):
            C[kappa] = np.where(J==kappa)[0]
        # update cluster medoids
        for kappa in range(k):
            J = np.mean(D[np.ix_(C[kappa],C[kappa])],axis=1)
            j = np.argmin(J)
            Mnew[kappa] = C[kappa][j]
        np.sort(Mnew)
        # check for convergence
        if np.array_equal(M, Mnew):
            break
        M = np.copy(Mnew)
    else:
        # final update of cluster memberships
        J = np.argmin(D[:,M], axis=1)
        for kappa in range(k):
            C[kappa] = np.where(J==kappa)[0]

    # return results
    return M, C

def get_kmedoids_labels(X, n_clusters):
    X_distance = pairwise_distances(X)
    X_medoids, X_clusters = kMedoids(X_distance, n_clusters)
    
    X_labels = []
    
    for label in X_clusters:
        for obs_idx in X_clusters[label]:
            X_labels.append([label, X[obs_idx]])
            
    return X_labels
    
# FIXME: Implement h-clustering algorithm + remove "hclust_labels" fn arg    

def get_cluster_labels(X, hclust_labels, n_clusters = 6):
    all_labels = {'kmeans' : KMeans(n_clusters = n_clusters).fit_predict(X),
                 'hclust' : hclust_labels,
                 'kmedoids' : sort_kmeds(get_kmedoids_labels(X.to_numpy(), n_clusters))}

    labels_df = pd.DataFrame(all_labels)
    return labels_df

# FIXME: Implement h-clustering algorithm + remove "hclust_labels" fn arg 

def get_silhouette_per_event(X, hclust_labels, n_clusters = 6):
    #n_clusters = 6
    
    kmeans_cluster = KMeans(n_clusters = n_clusters)
    kmeans_labels = kmeans_cluster.fit_predict(X)
    kmeans_silhouettes = silhouette_samples(X, kmeans_labels)
    
    #hclust_labels = get_hclust_labels(X, n_clusters)
    hclust_silhouettes = silhouette_samples(X, hclust_labels)
    
    kmed_labels = get_kmedoids_labels(X.to_numpy(), n_clusters)
    kmed_sorted = sort_kmeds(kmed_labels)
    kmed_silhouettes = silhouette_samples(X, kmed_sorted)

    return [kmeans_silhouettes, hclust_silhouettes, kmed_silhouettes]

def sort_kmeds(kmed_labels):
    kmed_labels_df = pd.DataFrame.from_records(kmed_labels, columns = ['kmed_label', 'event_idx'])
    kmed_labels_sorted = kmed_labels_df.sort_values(by = "event_idx")
    kmed_labels_array = kmed_labels_sorted["kmed_label"]
    return kmed_labels_array.to_numpy()

def get_all_labels(X, hclusters, n_clusters = 6):
    return pd.DataFrame({'kmeans' : KMeans(n_clusters = n_clusters).fit_predict(X),
                         'hclust' : hclusters[n_clusters],
                         'kmedoids' : sort_kmeds(get_kmedoids_labels(X.to_numpy(), n_clusters))})

def get_all_silhouettes(X, labels):
    #all_labels = get_all_labels(X, hclust_labels, n_clusters)
    return pd.DataFrame({'kmeans_silh' : silhouette_samples(X, labels["kmeans"]),
                         'hclust_silh' : silhouette_samples(X, labels["hclust"]),
                         'kmedoids_silh' : silhouette_samples(X, labels["kmedoids"])})

def get_labels_and_silhouettes(X, hclust_labels, n_clusters = 6):
    labels_df = get_all_labels(X, hclust_labels, n_clusters)
    silh_df = get_all_silhouettes(X, labels_df)
    labels_and_silh = pd.concat([labels_df, silh_df], axis=1, sort=False)
    return labels_and_silh

def get_kmeans_ensemble(X, n_clusters = 6, n_runs = 100):
    ensemble_list = pd.DataFrame()
    for i in range(n_runs):
        ensemble_list = pd.concat([ensemble_list, pd.DataFrame(KMeans(n_clusters).fit_predict(X))], axis=1, sort=False)
    return ensemble_list

def get_kmeds_ensemble(X, n_clusters = 6, n_runs = 100):
    ensemble_list = pd.DataFrame()
    for i in range(n_runs):
        ensemble_list = pd.concat([ensemble_list, pd.DataFrame(sort_kmeds(get_kmedoids_labels(X.to_numpy(), n_clusters)))], axis=1, sort=False)
    return ensemble_list

# FIXME: converge k-means fns below this fixme with fns above 

def get_cdist(df, clust_centers):
    return (cdist(df, clust_centers, 'euclidean'))  # axis = 1

def find_np_min(df, clust_centers):
    return (np.min(get_cdist(df, clust_centers)), 1)

def sum_np_min_cdist(df, clust_centers):
    return [sum(find_np_min(df, clust_centers)) / df.shape[0]]

def get_kmeans_distortions(df, kmeans_model):
    #return (sum(np.min(cdist(df, kmeans_models.cluster_centers_, 'euclidean'), axis = 1)) / df.shape[0])
    return sum_np_min_cdist(df, kmeans_model.cluster_centers_)

def return_cluster_indices(kmeans_model, k):
    """
        k = cluster number to check 
        kmeans_model = (after KMeans(...).fit(df)) predicted labels on data (i.e. df)
        
        This function can be used to return the indices of our original data 
        that kmeans_model predicted to be of cluster-id = k 
        
        E.g. > return_cluster_indices(3, kmeans_model) 
        
            Will return the indices of the original data that were clustered 
            to cluster-id = 3 (i.e. the 3rd cluster of K clusters total)
    """
    return (np.where(k == kmeans_model.labels_)[0])

def return_n_kmeans(df, n = 5):
    """
        n = number of k-means models to generate
            with number-of-clusters from 1:n 
            
        This function returns a 2-d array: 
            index-0 | k-means model 
            index-1 | respective labels of model on data (predict)
            index-2 | respective cluster centroid distances (distortions)
    """
    k_models = []
    model_predicts = []
    distortions = []
    n_iters = range(1, n)
    for k in n_iters:
        print(k)
        
        ## Generate kmeans for number-of-clusters = k
        k_models.append(get_kmeans_model(df, k))
        
        ## Predict kth kmeans model on data 
        model_predicts.append(get_kmeans_labels(df, k_models[(k - 1)]))
        
        ## Calculate cluster centroid distances (distortions)
        distortions.append(get_kmeans_distortions(df, k_models[(k - 1)]))
        
    return [k_models, model_predicts, distortions]
        
########################################
        
